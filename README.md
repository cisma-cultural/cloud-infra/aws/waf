# WAF Stack

This stack is used to create the base WAF infrastructure. It is currently not used because the existing `finances` deployment requires an ALB with a certificate; certificate generation is currently out of scope since cisma's DNS is managed outside AWS.
