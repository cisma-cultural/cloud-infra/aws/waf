import * as cdk from "aws-cdk-lib";
import {Template} from "aws-cdk-lib/assertions";
import {WafStack} from "../lib/waf-stack";

describe("waf stack", () => {
  let app: cdk.App;
  let stack: WafStack;

  let template: cdk.assertions.Template;

  beforeEach(() => {
    app = new cdk.App();
    stack = new WafStack(app, "WafStackTest");

    template = Template.fromStack(stack);
  });

  test("should create waf", () => {
    const webACLResource = template.findResources("AWS::WAFv2::WebACL");
    expect(Object.keys(webACLResource)).toHaveLength(1);
    expect(webACLResource).toMatchSnapshot();
  });

  test("should create expected outputs", () => {
    template.hasOutput("baseWafAclId", {
      Value: {
        "Fn::GetAtt": [
          "BaseWebAcl",
          "Id"
        ]
      }
    });
    template.hasOutput("baseWafAclName", {
      Value: "BaseWebAcl"
    });
    template.hasOutput("baseWafAclArn", {
      Value: {
        "Fn::GetAtt": [
          "BaseWebAcl",
          "Arn"
        ]
      }
    });
  });
});
