import * as cdk from "aws-cdk-lib";
import {Construct} from "constructs";
import {aws_wafv2 as wafv2, CfnOutput} from "aws-cdk-lib";

export class WafStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const baseWebAcl = new wafv2.CfnWebACL(
      this,
      "BaseWebAcl",
      {
        defaultAction: {
          allow: {}
        },
        scope: "REGIONAL",
        visibilityConfig: {
          cloudWatchMetricsEnabled: true,
          metricName: "WafMetricForBaseWebACL",
          sampledRequestsEnabled: true,
        },
        name:"BaseWebAcl",
        rules: [{
          name: "CRSRule",
          priority: 0,
          statement: {
            managedRuleGroupStatement: {
              name: "AWSManagedRulesCommonRuleSet",
              vendorName: "AWS"
            }
          },
          visibilityConfig: {
            cloudWatchMetricsEnabled: true,
            metricName: "WafMetricForBaseWebACL-CRS",
            sampledRequestsEnabled: true,
          },
          overrideAction: {
            none: {}
          },
        }]
      }
    );

    new CfnOutput(this, 'baseWafAclId', {
      value: baseWebAcl.attrId
    });
    new CfnOutput(this, 'baseWafAclName', {
      value: baseWebAcl.name!
    });
    new CfnOutput(this, 'baseWafAclArn', {
      value: baseWebAcl.attrArn,
      exportName: 'baseWafAclArn'
    });
  }
}
